﻿Imports System.Data.Odbc

Public Class Form3
    Sub TampilGrid()
        Call koneksi()
        da = New OdbcDataAdapter("select * From tb_pegawai", conn)
        ds = New DataSet
        ds.Clear()
        da.Fill(ds, "tb_pegawai")
        DataGridView1.DataSource = ds.Tables("tb_pegawai")
        DataGridView1.Columns("id").Visible = False
        DataGridView1.Columns("nama").Width = 200
        DataGridView1.ReadOnly = True
    End Sub

    Private Sub Form3_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Call TampilGrid()

    End Sub

    Private Sub btnKembali_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnKembali.Click
        FormHome.Show()
        Me.Close()
    End Sub

    Private Sub EditDataPegawaiToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles EditDataPegawaiToolStripMenuItem.Click
        Me.Close()
        Form1.Show()
    End Sub

    Private Sub UpdatePenggajianToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles UpdatePenggajianToolStripMenuItem.Click
        Me.Close()
        Form2.Show()
    End Sub

    Private Sub LihatDatabaseToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles LihatDatabaseToolStripMenuItem.Click
        Me.Show()
    End Sub

    Private Sub KeluarToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles KeluarToolStripMenuItem.Click
        If MessageBox.Show("Apakah Anda Yakin Ingin Keluar ?", "Keluar Aplikasi", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
            FormHome.Close()
            FormLogin.Close()
        End If
    End Sub

    Private Sub HomeToolStripMenuItem_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles HomeToolStripMenuItem.Click
        Me.Close()
        FormHome.Show()
    End Sub
End Class